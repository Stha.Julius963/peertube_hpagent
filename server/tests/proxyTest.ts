const assert = require("chai").assert
const proxyTest = require("../middlewares/proxy")

describe("the Proxy function", function () {
  if (proxyTest()) {
    it("value from proxy is not null", function () {
      const result = proxyTest()
      assert.isNotNull(result, "proxy value is not null")
    })
    it("value from proxy exits", function () {
      const result = proxyTest()
      assert.exists(result, "proxy value exists")
    })
    it("object should return type string", function () {
      const result = proxyTest()
      assert.typeOf(result, "string")
    })
  } else {
    it("value from proxy is  undefined", function () {
      const result = proxyTest()
      assert.isUndefined(result, "proxy value is undefined")
    })
  }
})
