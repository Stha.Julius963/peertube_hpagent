
const envs = require("env-smart")

// read environment variable

const OS_HTTP_PROXY = envs.load().HTTP_PROXY
const OS_HTTPS_PROXY = envs.load().HTTPS_PROXY

const proxy = () => {
  if (typeof OS_HTTPS_PROXY !== "undefined" && OS_HTTPS_PROXY) {
    // console.log("OS HTTPS_PROXY Environment variable :", `${OS_HTTPS_PROXY}`)
    return OS_HTTPS_PROXY
  } else if (typeof OS_HTTP_PROXY !== "undefined" && OS_HTTP_PROXY) {
    // console.log("OS HTTP_PROXY Environment variable :", `${OS_HTTP_PROXY}`)
    return OS_HTTP_PROXY
  } else {
    console.log("Proxy has not been set.")
  }
}

module.exports = proxy
